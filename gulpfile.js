'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const connect = require('gulp-connect');

const paths = {
  demo : './demo',
  dist : './dist',
  sass : './src/**/*.scss',
  test : './tests/*.js',
  js   : './src/**/*.js',
  html : './demo/*.html'
}

gulp.task('default', ['js', 'sass']);
gulp.task('start', ['sass:demo', 'js:demo', 'serve', 'watch']);

gulp.task('sass', () => {
  return gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.dist));
});

gulp.task('sass:demo', () => {
  return gulp.src(paths.sass)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(paths.demo))
    .pipe(connect.reload());
});

gulp.task('js', () => {
  return gulp.src(paths.js)
    .pipe(gulp.dest(paths.dist));
});

gulp.task('js:demo', () => {
  return gulp.src(paths.js)
    .pipe(gulp.dest(paths.demo))
    .pipe(connect.reload());
});

gulp.task('html', () => {
  return gulp.src(paths.html)
    .pipe(connect.reload());
});

gulp.task('watch', () => {
  gulp.watch(paths.html, ['html']);
  gulp.watch(paths.js, ['js:demo']);
  gulp.watch(paths.sass, ['sass:demo']);
});

gulp.task('serve', () => {
  connect.server({
    name: 'Demo app',
    root: ['demo', 'test'],
    port: 8000,
    livereload: true
  });
});
