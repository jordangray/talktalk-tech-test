TalkTalk accordion demo
=======================


Prerequisites
-------------

The minimum prerequisites to use this accordion are:

- **AngularJS 1.5+**.
- **ngAria** (for keyboard accessibility).
- The **TalkTalk icon font** provided with this test.

For the purposes of the demo page, I also used the Open Sans webfont provided and Bootstrap 3, direct from the CDN. Neither of these are required to use the accordion component. All code was tested on Node.js 6.2.2.


Installing the accordion
------------------------

Add the accordion CSS to your document head:

    <link rel="stylesheet" href="dist/tt-accordion.css">
    <link rel="stylesheet" href="/fonts/icon-font/TalkTalkIcons.css">

Then add the accordion JS after the references to AngularJS and ngAria:

    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-aria.js"></script>
    <script src="dist/tt-accordion.js"></script>


Create an accordion
-------------------

To create an accordion, use the following structure:

    <tt-accordion-group>
      <tt-accordion header="The accordion header" open="true|false" sticky="true|false">
        [Accordion content]
      </tt-accordion>
    </tt-accordion-group>

The accordion attributes are:

<dl>
  <dt>`header`</dt>
  <dd>The header text used for this accordion.</dd>

  <dt>`open`</dt>
  <dd>_(Default: `false`.)_ If set to `true`, the accordion will be expanded by default; if `false`, it will be closed.</dd>

  <dt>`sticky`</dt>
  <dd>_(Default: `false`.)_ If set to `true`, the accordion header will stick to the top of the viewport while its content is still visible; if `false`, it will remain static and scroll normally with its content.</dd>
</dl>


Accessibility
-------------

I implemented some of the recommendations from the [WAI Authoring Practices Guide for accordions](https://www.w3.org/TR/wai-aria-practices/#accordion):

- Thanks to [ngAria](https://docs.angularjs.org/guide/accessibility), all accordion headers are focusable and respond to enter/spacebar keyboard events in addition to click events.
- Accordions have the recommended ARIA roles: `tablist` for the accordion group, `tab` for accordion headers and `tabpanel` for the accordion content.
- An expanded accordion is indicated by `aria-expanded="true"` on the accordion header and `aria-hidden="false"` on the accordion content.

Due to time constraints, I did **not** implement:

- The full list of recommended keyboard controls. (There are lots, some of dubious utility.)
- Using `aria-selected` to indicate the currently focused accordion.


Running the demo locally
------------------------

To run the demo locally, install the required packages from NPM and start it using Gulp.

    npm install
    npm start

The demo page is now visible on [localhost:8000](http://localhost:8000).


Running the tests
-----------------

Before you can run the tests, you should install Protractor and Webdriver:

    npm install -g protractor
    webdriver-manager update

Next, start the Selenium WebDriver server:

    webdriver-manager start

In another command window, start the Gulp Connect server:

    npm start

Finally, in another command window, we can run our tests:

    npm test

This will run Protractor using the configuration specified in `/test/config.js`.


Things I didn’t do
------------------

- I omitted **smooth opening and closing effects** for the accordions since these were not mentioned in the task description and I didn’t have much time.
- Though I was tempted, I ultimately didn't use **ES6 or browserify**. All my code is in a single ES5 file, which was messy but quick.
- There is no **minification** of the built code, which would be unacceptable for a production site.