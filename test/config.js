exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['**/*.js'],
  rootElement: 'main',
  jasmineNodeOpts: {
    print: () => {}
  },
  onPrepare() {
    const SpecReporter = require('jasmine-spec-reporter');
    jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'all'}));
    browser.driver.manage().window().setSize(600, 600);
  }
};
