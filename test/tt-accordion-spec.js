const testUrl = 'http://localhost:8000/test.html';

const selector = {
  open      : '#protractor-open',
  closed    : '#protractor-closed',
  sticky    : '#protractor-sticky',
  group     : '.tt-accordion',
  accordion : '.tt-accordion__accordion',
  header    : '.tt-accordion__header',
  marker    : '.tt-accordion__marker',
  content   : '.tt-accordion__content'
};

describe('ttAccordion', () => {

  describe('open accordion', () => {
    it('should be open', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.open));
      const header = accordion.element(by.css(selector.header));
      const marker = header.element(by.css(selector.marker));
      const content = accordion.element(by.css(selector.content));

      expect(header.getAttribute('aria-expanded')).toEqual('true');
      expect(content.getAttribute('aria-hidden')).toEqual('false');
      expect(marker.getAttribute('class')).toContain('tt_icon-arrowup');
    });

    it('should close when the header is clicked', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.open));
      const header = accordion.element(by.css(selector.header));
      const marker = header.element(by.css(selector.marker));
      const content = accordion.element(by.css(selector.content));

      header.click();

      expect(header.getAttribute('aria-expanded')).toEqual('false');
      expect(content.getAttribute('aria-hidden')).toEqual('true');
      expect(marker.getAttribute('class')).toContain('tt_icon-arrowdown');
    });
  });


  describe('closed accordion', () => {
    it('should be closed', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.closed));
      const header = accordion.element(by.css(selector.header));
      const marker = header.element(by.css(selector.marker));
      const content = accordion.element(by.css(selector.content));

      expect(header.getAttribute('aria-expanded')).toEqual('false');
      expect(content.getAttribute('aria-hidden')).toEqual('true');
      expect(marker.getAttribute('class')).toContain('tt_icon-arrowdown');
    });

    it('should open when the header is clicked', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.closed));
      const header = accordion.element(by.css(selector.header));
      const marker = header.element(by.css(selector.marker));
      const content = accordion.element(by.css(selector.content));

      header.click();

      expect(header.getAttribute('aria-expanded')).toEqual('true');
      expect(content.getAttribute('aria-hidden')).toEqual('false');
      expect(marker.getAttribute('class')).toContain('tt_icon-arrowup');
    });
  });


  describe('sticky accordion', () => {
    it('should move header when scrolled past', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.sticky));
      const header = accordion.element(by.css(selector.header));

      header.click(); // Open the accordion

      header.getLocation().then(before => {
        browser.executeScript('window.scrollTo(0,' + (before.y + 100) + ')')
          .then(() => {
            header.getLocation().then(after => {
              expect(after.y).toEqual(before.y + 100);
            });
          });
      });
    });

    it('should not move header when accordion is closed', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.sticky));
      const header = accordion.element(by.css(selector.header));

      header.getLocation().then(before => {
        browser.executeScript('window.scrollTo(0,' + (before.y + 100) + ')')
          .then(() => {
            header.getLocation().then(after => {
              expect(after.y).toEqual(before.y);
            });
          });
      });
    });

    it('should scroll back up when header is clicked', () => {
      browser.get(testUrl);

      const accordion = element(by.css(selector.sticky));
      const header = accordion.element(by.css(selector.header));

      header.getLocation().then(before => {
        browser.executeScript('window.scrollTo(0,' + (before.y + 100) + ')')
          .then(() => {
            header.click();
            browser.executeScript('return window.pageYOffset').then((y) => {
              expect(y).toEqual(before.y);
            })
          });
      });
    });
  });

});
